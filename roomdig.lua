--[[
  Roomdig
  Program to dig rooms with given size
--]]

__author__  = 'Boisei0'
__version__ = '0.2a'

-- Variable declaration
local tArgs = {...}

function help ()
  print('Usage: ')
  print(shell.getRunningProgram() .. ' <length> <depth> <height>')
end

function chkParam ()
  if #tArgs ~= 3 then
    print('Wrong arguments given')
    help()
    return 0
  end
end

function error (msg)
  print('Error: ' .. msg)
end

function chkFuel ()
  if turtle.getFuelLevel < 1 then
    return turtle.refuel()
  else
    return true
  end
end

function digRow (depth)
  for i = 0, depth do
    if not chkFuel() then:
      print('Not enough fuel. Stopping...')
      return 0
    end
    if turtle.detect() then
      turtle.dig()
    end
    turtle.forward()
  end
end

function digLayer (length, depth) -- Will fail at the end of each layer; TODO: Fix later
  direction = 2 -- Direction facing: 0 = back, 1 = left, 2 = front, 3 = right
  for i = 0, length do
    if digRow(depth) == 0 then
      return 0
    end
    if direction == 2 then
      -- turn right, 1 step, turn right, start over
      turtle.turnRight()
      if turtle.detect() then
        turtle.dig()
      end
      turtle.forward()
      turtle.turnRight()
      direction = 0
    elseif direction == 0
      -- turn left, 1 step, turn left, start over
      turtle.turnLeft()
      if turtle.detect() then
        turtle.dig(
      end
      turtle.forward()
      turtle.turnRight()
      direction == 2
    end
  end
  -- TODO: move to starting point of layer
end

function digRoom (length, depth, hight) -- Will fail at the end of a room; TODO: Fix later
  for i = 0, hight do
    if diglayer(length, depth) == 0 then
      return 0
    end
    if turtle.detectUp() then
      turtle.digUp()
    end
    turtle.up()
  end
  -- TODO: move to starting point of room
end

function main ()
  if chkParam() == 0 then
    return 0
  end
  if digRoom (length, depth, hight) == 0 then
    return 0
  end
end

main()
