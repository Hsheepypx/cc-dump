--[[
  Dediget
  Program to get file from Boisei0's server and save it to disk
  Based on gitget: https://github.com/aacoba/CCDump/blob/master/gitget.lua
--]]

__author__ = 'Boisei0'
__version__ = '0.2.5a'

-- Variable declaration
local tArgs = {...}
local baseUrl = 'http://5.39.80.163/ftb/programs/'
local baseUrlTurtle = 'http://5.39.80.163/ftb/programs/turtle/'

function help ()
  print('Usage: ')
  print(shell.getRunningProgram() .. ' <filename> <turtle>')
  print('With <turtle> as boolean for turtle program.')
end

function chkParam ()
  if #tArgs ~= 2 then
    print('Wrong arguments given')
    help()
    return 0
  end
end

function error (msg)
  print('Error: ' .. msg)
end

function fileWrite (filestr) -- write string to file
  local sRpl = filestr.readAll()
  local file = fs.open(tArgs[1], 'w')
  file.write(sRpl)
  print('Download complete!')
  file.close()
end

function getProg ()
  url = ''
  if tArgs[2] ~= 'true' then
    url = baseUrl .. tArgs[1] .. '.lua'
  else
    url = baseUrlTurtle .. tArgs[1] .. '.lua'
  end
  local rpl = http.get(url)
  if rpl == nil then
    error('File does not exist')
    return 0
  end
  fileWrite(rpl)
  rpl.close()
end


function main ()
  if chkParam() == 0 then
    return 0
  end
  if getProg() == 0 then
    return 0
  end
end

main()
